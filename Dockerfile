FROM node:16.13.1-alpine

RUN mkdir -p /opt/plaanet-api
WORKDIR /opt/plaanet-api

# Copy only package.json and yarn.lock for cache
COPY package.json yarn.lock ./

# Install Dependncies
RUN yarn install --ignore-optional --pure-lockfile --non-interactive

# Copy Files
COPY . ./

# Run production API.
ENV NODE_ENV=production
ENTRYPOINT [ "node", "main.js" ]
