# plaanet-api

All installation process is here : https://gitlab.com/plaanet/plaanet-install

If you use Docker: 
- [Docker to develop Plaanet](./docs/docker.md)
- [Running a production node with Docker Compose](./docs/install-node-docker-compose.md)