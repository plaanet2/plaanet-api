module.exports = {
    async up(db, client) {
        // Create referrerUid field if it doesn't exists.
        await db.collection('pages')
            .updateMany({
                publicPGPKey: {
                    $exists: false
                }
            }, {
                $set: {
                    publicPGPKey: null,
                }
            });

    },

    async down(db, client) {}
};
