var express = require("express")
var router = express.Router()

const auth = require("../core/middleware/auth-node");
const EntityService = require("../services/entity");
const PageService = require("../services/page");

router.get('/ready', async (req, res) => {
  return res.send({ ready: true })
})

router.post('/get-entities', auth,  async (req, res) => {
  //console.log("--------------------------------")
  //console.log("/fast/get-entities", req.body.entityType, req.body.limit, req.body.skip, req.body.sort)

  if(req.body.entityType == null)
    return res.send({ error: true })
  
  let entityService = new EntityService()
  let pageService = new PageService()
  let { entities, totalRes } = await entityService.queryEntity(req.body)
  
  entities = pageService.afterGetMapPages(entities)
  //console.log("entities found:", entities.length, "pageUid", req.body.pageUid)

  let result = {  error: false, 
                  entities: entities, 
                  totalRes: totalRes,
                  origin: req.body.origin, 
                  key: req.body.key 
               }

  if(req.body.pageUid != null)
    req.ws.emit(req.body.pageUid, 'send-map-pages', result)
 
  return res.send(result)
})


module.exports = router;