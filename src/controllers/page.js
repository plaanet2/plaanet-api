var express = require("express")
var router = express.Router()
var app = express()
var cors = require('cors')

const auth = require("../core/middleware/auth");
const bcrypt = require("bcryptjs");
const { User, validate } = require('../models/User')
const Instance = require('../models/Instance')
const Publication = require('../models/Publication')
const Page = require('../models/Page')
const File = require('../models/File')
const PublicationService = require("../services/publication");
const EntityService = require("../services/entity");
const PageService = require("../services/page");

const config = require('config');
const _ = require('underscore');
const jwt = require('jsonwebtoken');

const axios = require('axios');
const core = require('../core/core');
var fs = require('fs');

router.post('/edit-description', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/page/edit-description")
    
    let page = await Page.findOne( { uid: req.body.pageUid, 
                                     owner: req.user._id } )
    if(page == null){
        res.json( { error: true } );
        return
    }
    page.description = req.body.description != "" ? req.body.description : null
    page.save()
    
    res.json( { error: false } );
})

router.post('/edit-tags', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/page/edit-tags")
    
    let page = await Page.findOne( { type: "user",
                                     owner: req.user._id } )
    if(page == null){
        res.json( { error: true } );
        return
    }
    page.hashtags = req.body.tags
    page.hashtagsG = req.body.tagsG
    page.hashtagsC = req.body.tagsC
    page.hashtagsM = req.body.tagsM 
    page.save()
    
    res.json( { error: false } );
})

router.post('/edit-position', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/page/edit-position")

    let today = new Date()
    today.setHours(0,0,0,0);
        
    let page = await Page.findOne( { uid: req.body.pageUid,
                                     owner: req.user._id } )

    console.log("dateLastPosition", page.dateLastPosition.toString())
    console.log("created", page.created.toString())
    console.log("page", page.name)

    let last = new Date(page.dateLastPosition)
    if(page == null
        || (last > today 
            && page.created.toString() != page.dateLastPosition.toString())
    ){
        res.json( { error: true } );
        return
    }
    page.coordinates = [parseFloat(req.body.lng), parseFloat(req.body.lat)]
    page.city = req.body.city
    page.address = req.body.address
    //page.created = new Date()
    page.dateLastPosition = new Date()
    page.save()
    
    res.json( { error: false, page: page } );
})


router.post('/get-stream', auth,  async (req, res) => {
    //console.log("-------------------------")
    //console.log("/page/get-stream") //, req.body)
    
    let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )
    
    let nbPerPage = 10

    let params = { 
            userId : req.user._id, //id de l'utilisateur qui veut afficher le stream
            pageUid : req.body.pageUid, //id de la page dont on veut afficher le stream
            userPageUid : userPage.uid,
            radius : req.body.radius,
            lat: req.body.lat,
            lng: req.body.lng,
            pageNumber: req.body.pageNumber,
            nbPerPage: nbPerPage,
            search: req.body.search,
            userPageCoordinates: userPage.coordinates,
        }

    //console.log("try contact", '/broadcast/get-stream', req.body.pageUid)
    core.broadcaster.fastAsync('/broadcast/get-stream', params,
        function(dataRes){ // == then (after response from all instances)
            
        },function(result){ // == catch
            console.log("error 2 : fCatch", result)
        }
        ,'stream'
        , true) //dataRes[id] to concat for each response after forward

    let publicationService = new PublicationService()
    let streamRes = await publicationService.getStream(params)

    //console.log("streamRes", streamRes.stream.length)
    return res.json(streamRes)

})

//return a list of pages located in circle area
router.post('/get-map-pages', auth,  async (req, res) => {
    //console.log("-------------------------")
    //console.log("/page/get-map-pages", req.body.pageType, req.body.nbPerPage)
    
    let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )
    userPage.dateConnected = new Date()
    userPage.save()

    let params = req.body
    params.pageUid = userPage.uid
    params.skip = (req.body.pageNumber >= 1) ? (req.body.pageNumber - 1) : 0
    params.nbPerPage = (req.body.nbPerPage != null) ? (req.body.nbPerPage) : 50 

    let pageService = new PageService()
    let qry = pageService.getMapQuery(params)

    let paramsE = { qry : qry }

    let eService = new EntityService()
    let after = pageService.afterGetMapPages
    try{
        eService.getEntities('page', paramsE, after)
                .then((response)=>{
                    //console.log("response", response.length)
                    let finalRes = response
                    finalRes.mapPages = response.entities
                    delete finalRes.entities
                    res.json(finalRes)
                })
                .catch((error)=>{
                    res.json({error: error})
                })
    }catch(e){
        console.log("/page/get-map-pages ERROR", e) 
        res.json({error: e})
    }
})


//return a list of pages located in circle area
router.post('/get-map-pages/desync', auth,  async (req, res) => {
    //console.log("-------------------------")
    //console.log("/page/get-map-pages/desync", req.body.pageType, req.body.nbPerPage, req.body.origin)
    
    let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )
    userPage.dateConnected = new Date()
    userPage.save()

    let params = req.body
    params.pageUid = userPage.uid
    params.skip = (req.body.pageNumber >= 1) ? (req.body.pageNumber - 1) : 0
    params.nbPerPage = (req.body.nbPerPage != null) ? (req.body.nbPerPage) : 50 

    let pageService = new PageService()
    let qry = pageService.getMapQuery(params)

    let paramsE = { qry : qry, 
                    search: req.body.search,
                    origin: req.body.origin, 
                    key: req.body.key, 
                    fromPageUid: params.pageUid 
                  }

    let eService = new EntityService()
    let after = pageService.afterGetMapPages
    try{
        //console.log("eService.getEntitiesAsync", paramsE) 
        eService.getEntitiesAsync('page', paramsE, after)
    }catch(e){
        console.log("/page/get-map-pages ERROR in getEntitiesAsync", e) 
        //res.json({error: e})
    }

    qry.entityType = 'page'

    //let totalRes = await Page.countDocuments(qry.query)
    let { entities, totalRes } = await eService.queryEntity(qry)
    let mapPages = after(entities)
    //console.log("/page/get-map-pages END", params, mapPages.length)

    res.json({  error: false, 
                mapPages: mapPages, 
                nbPerPage: params.nbPerPage,
                totalRes: totalRes })

})


//return a list of pages located in circle area
router.post('/get-map-assembly', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/page/get-map-assembly", req.body)
    
    //let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )

    try{ //broadcast request (to each instance of the network)
        // nbPerPage = 100
        core.broadcaster.post('/broadcast/get-map-assembly', { }, //params
            function(dataRes){ // == then (after response from all instances)
                //if network sent data
                if(dataRes != null){
                    //let finalRes = _.sortBy(finalRes, 'created').reverse()
                    res.json({  error: false, 
                                map: dataRes })
                }else{
                    console.log("error 1 : dataRes == null")
                    res.json({ error: true, map: null, 
                            errorMsg: 'No page found in this bdd' })
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
                //res.json({ error: true, wallets: null, 
                           //errorMsg: 'An error occured', result: result })
            },  
            'map') //dataRes[id] to concat for each response after forward
    }catch(e){
        console.log("error 3")
        res.json({ error: true, mapPages: null, e:e })
    }
   
})

//return a number of pages located in circle area
router.post('/get-count-pages', auth,  async (req, res) => {
    //console.log("-------------------------")
    //console.log("/page/get-count-pages", req.body)
    try{ //broadcast request (to each instance of the network)
        let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )
        core.broadcaster.post('/broadcast/get-count-pages', 
            { radius : req.body.radius,
              lat: req.body.lat,
              lng: req.body.lng,
              pageNumber: req.body.pageNumber,
              search: req.body.search,
              pageType: req.body.pageType,
              userPageUid: userPage.uid
            }, //params
            function(dataRes){ // == then (after response from all instances)
                //if network sent data
                if(dataRes != null){
                    let countTotal = 0
                    dataRes.forEach((v, i)=>{
                        //console.log("get-count-pages total", v, countTotal)
                        countTotal += v
                    })
                    res.json({  error: false, count: countTotal })
                }else{
                    console.log("error 1 : dataRes == null")
                    res.json({ error: true, count: null, 
                                errorMsg: 'No page found in this bdd' })
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch")
                //res.json({ error: true, wallets: null, 
                           //errorMsg: 'An error occured', result: result })
            },  
            'count') //dataRes[id] to concat for each response after forward
    }catch(e){
        console.log("error 3")
        res.json({ error: true, stream: null, e:e })
    }
   
})

router.post('/profil', auth,  async (req, res) => {
    //console.log("-------------------------")
    //console.log("/page/profil", req.body.pageUid)
    
    try{ 
        let nodePath = await core.getNodePath(req.body.pageUid)
        const i = await Instance.findOne({isLocal: true}) 
        let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )

        if(core.iName(req.body.pageUid) == i.name){
            let pageService = new PageService()
            let page = await pageService.getPageProfile(req.body.pageUid, userPage.coordinates)

            if(page == null) return res.json({ error: true, page: null })

            let ownerPage = null
            if(page.type != "user"){
                ownerPage = await Page.findOne({ owner: page.owner._id, type: "user" }).select(["uid", "name"])
            }
            
            return res.json({ error: false, page: page, ownerPage: ownerPage })
        }
        
        core.post(nodePath + '/broadcast/page-profil', 
            { pageUid : req.body.pageUid, userPageCoordinates: userPage.coordinates }, 
            async function(dataRes){ 
                if(dataRes != null && dataRes.data.error == false){
                    res.json({  error: false, 
                                page: dataRes.data.page, 
                                ownerPage: dataRes.data.ownerPage })
                }else{
                    console.log("error 1 : dataRes == null")
                    res.json({ error: true, page: null, 
                            errorMsg: 'No page found in this bdd' })
                }
            },function(result){ // == catch
                console.log("error 2 : fCatch /broadcast/page-profil")
            })
    }catch(e){
        console.log("error 3", e)
        res.json({ error: true, page: null, e:e })
    }
   
})

router.post('/add-to-blacklist', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/page/add-to-blacklist", req.body.pageUid)
    
    let page = await Page.findOne( { owner: req.user._id, type: "user" } )
    if(page == null){
        res.json( { error: true } );
        return
    }
    if(page.blacklist == null) page.blacklist = []
    
    let exist = false
    page.blacklist.forEach((c) => {
        if(c.uid == req.body.pageUid) exist = true
    })  
    if(exist) return res.json( { error: true } );

    page.blacklist.push({ uid: req.body.pageUid, 
                          name: req.body.pageName })
    page.save()
    
    res.json( { error: false } );
})

router.post('/remove-from-blacklist', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/page/remove-from-blacklist")
    
    let page = await Page.findOne( { owner: req.user._id, type: "user" } )
    if(page == null){
        res.json( { error: true } );
        return
    }

    if(page.blacklist == null) page.blacklist = []
    let pos = -1
    page.blacklist.forEach((p, i)=>{
        if(p.uid == req.body.pageUid) pos = i
    })
    if(pos > -1) page.blacklist.splice(pos, 1)
    
    page.save()
    
    res.json( { error: false } );
})


router.post('/add-to-follows', auth,  async (req, res) => {
    //console.log("-------------------------")
    //console.log("/page/add-to-follows")
    
    let page = await Page.findOne( { owner: req.user._id, type: "user" } )
    if(page == null){
        res.json( { error: true } );
        return
    }
    if(page.follows == null) page.follows = []
    
    let exist = false
    page.follows.forEach((c) => {
        if(c.uid == req.body.pageUid) exist = true
    })  
    console.log("already in follows", exist)
    if(exist) return res.json( { error: true } );

    page.follows.push({ uid: req.body.pageUid, 
                          name: req.body.pageName })

    try{ //broadcast new follower
        let nodePath = await core.getNodePath(req.body.pageUid)
        let userPage = await Page.findOne({ owner: req.user._id, type: 'user' })

        // console.log("try contact", nodePath + '/broadcast/page-add-follower', 
        //             req.body.type, req.body.postUid)

        core.post(nodePath + '/broadcast/page-add-follower', 
            {   pageUid : req.body.pageUid,
                pageName : req.body.pageName,
                userPageUid: userPage.uid
            }, //params
            function(dataRes){ // == then (after response from all instances)
                //console.log("dataRes", dataRes.data)
                //return res.json(dataRes.data)                    
            },function(e){ // == catch
                console.log("error 2 : fCatch", e)
            })
    }catch(e){
        console.log("error 3")
        res.json({ error: true, e:e })
    }        

    page.save()
    res.json( { error: false } );
})

router.post('/remove-from-follows', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/page/remove-from-follows")
    
    let page = await Page.findOne( { owner: req.user._id, type: "user" } )
    if(page == null){
        res.json( { error: true } );
        return
    }

    if(page.follows == null) page.follows = []
    let pos = -1
    page.follows.forEach((p, i)=>{
        if(p.uid == req.body.pageUid) pos = i
    })
    if(pos > -1) page.follows.splice(pos, 1)
    
    try{ //broadcast remove follower
        let nodePath = await core.getNodePath(req.body.pageUid)
        let userPage = await Page.findOne({ owner: req.user._id, type: 'user' })

        //console.log("try contact", nodePath + '/broadcast/page-remove-follower', 
        //            req.body.type, req.body.postUid)

        core.post(nodePath + '/broadcast/page-remove-follower', 
            {   pageUid : req.body.pageUid,
                userPageUid: userPage.uid
            }, //params
            function(dataRes){ // == then (after response from all instances)
                //console.log("dataRes", dataRes.data)
                //return res.json(dataRes.data)                    
            },function(e){ // == catch
                console.log("error 2 : fCatch", e)
            })
    }catch(e){
        console.log("error 3")
        res.json({ error: true, e:e })
    }        

    page.save()
    
    res.json( { error: false } );
})

router.post('/add-to-contacts', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/page/add-to-contacts")
    
    let page = await Page.findOne( { owner: req.user._id, type: "user" } )
    //console.log("page", page, req.user._id)
    if(page == null){
        res.json( { error: true } );
        return
    }
    if(page.contacts == null) page.contacts = []

    let exist = false
    page.contacts.forEach((c) => {
        //console.log(c.uid, "?==", req.body.pageUid)
        if(c.uid == req.body.pageUid) exist = true
    })  
    console.log("page exist", exist, req.body.pageUid, req.body.pageName)
    if(exist) return res.json( { error: true, msg: "already in contact" } );

    if(req.body.pageUid == null || req.body.pageName == null) 
        return res.json( { error: true } );

    page.contacts.push({ uid: req.body.pageUid, 
                          name: req.body.pageName })
    page.save()
    
    res.json( { error: false } );
})

router.post('/remove-from-contacts', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/page/remove-from-contacts")
    
    let page = await Page.findOne( { owner: req.user._id, type: "user" } )
    if(page == null){
        res.json( { error: true } );
        return
    }

    if(page.contacts == null) page.contacts = []
    let pos = -1
    page.contacts.forEach((p, i)=>{
        if(p.uid == req.body.pageUid) pos = i
    })
    if(pos > -1) page.contacts.splice(pos, 1)
    
    page.save()
    
    res.json( { error: false } );
})


router.post('/create-page', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/page/create-page")

    let user = await User.findById(req.user._id);
    let userPage = await Page.findOne( { owner: req.user._id, type: 'user' } )
    
    const i = await Instance.findOne({ isLocal:true })
    console.log(">>>", req.body.coordinates == null, i.position.coordinates, userPage)
    let coords = req.body.coordinates == null ? userPage.coordinates : userPage.coordinates

    let page = new Page()
    page.name = req.body.name
    page.description = req.body.description
    page.type = req.body.type
    page.owner = req.user._id
    page.slug = core.getSlug(page.name)
    page.nodeSlug = i.name + ":" + page.slug
    page.uid = i.name + ":" + page.id
    page.coordinates = coords
    page.blacklist = []
    page.follows = []

    let id = { uid: userPage.uid, name: userPage.name }
    page.followers = [userPage.uid]

    userPage.follows.push({ uid: page.uid, name: page.name })
    userPage.save()

    page.roles =        { 'admin': [id], 'moderator': [id], 'editor': [id] }
    page.pendingRoles = { 'admin': [],   'moderator': [],   'editor': [] }
    page.inviteRoles =  { 'admin': [],   'moderator': [],   'editor': [] }

    let now = new Date()
    page.created = now
    page.dateLastPosition = now

    if(req.body.startDate != null)
        page.startDate = req.body.startDate

    await page.save()

    const publicPath = config.get('public_folder_path') || './public';
    fs.copyFile(`${publicPath}/default-${page.type}.png`, 
                `${publicPath}/uploads/avatar/` + page.uid + '.png', 
                (err) => {
        if (err) throw err;
        console.log('default-avatar.png was copied to ' + page.uid + '.png');
    });

    user.pages.push(page)
    await user.save()
    await page.save()
    
    res.json( { error: false, page: page } );
})


router.post('/update-date-read-notif', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/page/update-date-read-notif")
    let page = await Page.findOne( { type: "user", 
                                     owner: req.user._id } )
    if(page == null){
        res.json( { error: true } );
        return
    }
    let now = new Date()
    page.dateReadNotif = now
    page.save()

    res.json( { error: false, date: now } );
})


router.post('/update-date-read-private', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/page/update-date-read-private")
    let page = await Page.findOne( { type: "user", 
                                     owner: req.user._id } )
    if(page == null){
        res.json( { error: true } );
        return
    }
    let now = new Date()
    page.dateReadPrivate = now
    page.save()

    res.json( { error: false, date: now } );
})


router.post('/get-library', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/page/get-library", req.body.pageUid)

    let nodePath = await core.getNodePath(req.body.pageUid)
    //let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )
    
    // console.log("try contact", nodePath + '/broadcast/page-get-library', req.body.pageUid)
    core.post(nodePath + '/broadcast/page-get-library', 
        { pageUid : req.body.pageUid,
        }, //params
        function(dataRes){ // == then (after response from all instances)
            //if network sent data
            if(dataRes != null && dataRes.data.error == false){
                //sort all res by "created" date
                //console.log("dataRes ok", dataRes.data.length)
                //dataRes = _.sortBy(dataRes, 'created').reverse()
                res.json({  error: false, 
                            files: dataRes.data.files })
            }else{
                console.log("error 1 : dataRes == null")
                res.json({ error: true, files: null, 
                        errorMsg: 'No files found in this bdd' })
            }
                
        },function(result){ // == catch
            console.log("error 2 : fCatch", result)
        })
})


router.post('/save-settings', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/page/save-settings", req.body.pageUid)

    let page = await Page.findOne( { uid: req.body.pageUid, 
                                     owner: req.user._id } )



    if (page == null){
        res.json( { error: true } );
        return
    }

    if (req.body.pageType == 'user') {
        page.publicPGPKey = req.body.publicPGPKey;
    }
    page.name = req.body.pageName != "" ? req.body.pageName : page.name
    page.type = req.body.pageType != "" ? req.body.pageType : page.type
    page.save()

    res.json( { error: false } );

})


router.post('/delete', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/page/delete", req.body.pageUid)

    let userPage = await Page.findOne( { owner: req.user._id, type: "user" } ).populate('owner')
    let query = { uid: req.body.pageUid }
    //si l'utilisateur qui veut supprimer une page est superAdmin de l'instance, il peut supprimer la page
    //ou sinon, seul le créateur (owner) d'une page peut la supprimer
    if(userPage.owner.isAdmin != true) query.owner = req.user._id

    let page = await Page.findOne( query )
    if(page == null){
        console.log('error : no page found for uid', req.body.pageUid, query)
        res.json( { error: true } );
        return
    }

    const pageType = page.type 
    await core.deleteCascade.page({ obj: page }, req)

    if(pageType == "user" && userPage.owner.isAdmin != true){
        let user = await User.findById(req.user._id)
        user.remove()
    }

    res.json( { error: false } );
})


router.post('/get-user-autocomplete', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/page/get-user-autocomplete", req.body.pageUid)

    try{ //broadcast request (to each instance of the network)
        // nbPerPage = 100
        core.broadcaster.post('/broadcast/page-get-autocomplete', 
            { search: req.body.search,
              pageType: "user"
            }, //params
            function(dataRes){ // == then (after response from all instances)
                //if network sent data
                if(dataRes != null){
                    //let finalRes = _.sortBy(finalRes, 'created').reverse()
                    res.json({  error: false, 
                                userPages: dataRes
                            })
                }else{
                    console.log("error 1 : dataRes == null")
                    res.json({ error: true, userPages: null })
                }
                    
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
                //res.json({ error: true, wallets: null, 
                           //errorMsg: 'An error occured', result: result })
            },  
            'pages') //dataRes[id] to concat for each response after forward
    }catch(e){
        console.log("error 3")
        res.json({ error: true, userPages: null, e:e })
    }
})


router.post('/manage-role', auth,  async (req, res) => {
    console.log("-------------------------")
    console.log("/page/manage-role", req.body.onPageUid)

    try{
        let nodePath = await core.getNodePath(req.body.onPageUid)
        let userPage = await Page.findOne( { owner: req.user._id, type: "user" } )
        
        // console.log("try contact", nodePath + '/broadcast/manage-role', req.body.onPageUid)
        core.post(nodePath + '/broadcast/manage-role', {
            type: req.body.type,            //action = add or cancel
            action: req.body.action,        //action = add, invite, pending
            role: req.body.role,            //role = admin editor moderator
            onPageUid: req.body.onPageUid,  //la page dont on gère les rôles (assemblée Truc)
            page: req.body.page,            //la page à qui on assigne un role (admin, editor, etc)
            userPageUid: userPage.uid,      //l'identité de celui qui effectue l'action
        }, //params
            async function(dataRes){ // == then (after response from all instances)
                //if network sent data
                if(dataRes != null && dataRes.data.error == false){
                    //sort all res by "created" date
                    //console.log("dataRes ok", dataRes.data)

                    if(dataRes.data.pageType == "user" 
                    && req.body.action == "role"
                    && req.body.role == "editor"){
                        let nodePath = await core.getNodePath(req.body.page.uid)
                        core.post(nodePath + '/broadcast/manage-role', {
                            type: req.body.type,                //action = add or cancel
                            action: req.body.action,            //action = add, invite, pending
                            role: req.body.role,                //role = admin editor moderator
                            onPageUid: req.body.page.uid,            //la page dont on gère les rôles (assemblée Truc)
                            page: { uid: req.body.onPageUid, name: dataRes.data.pageName },                     //la page à qui on assigne un role (admin, editor, etc)
                            userPageUid: req.body.onPageUid,     //l'identité de celui qui effectue l'action
                        },
                        (dataRes)=>{
                            console.log("final dataRes ok", dataRes.data)
                        },
                        (result)=>{
                            console.log("final dataRes error", result)
                        })

                    }
                    console.log("SEND NOTIF FOR NEW_ROLE_ACCEPTED ???")
                    if(req.body.type == 'add' && req.body.action == "role"){
                        console.log("SEND NOTIF FOR NEW_ROLE_ACCEPTED")
                        core.notify.send(req, {
                            verb: "NEW_ROLE_ACCEPTED",
                            authors: [{ uid: userPage.uid,
                                        name: userPage.name }],
                            whatObj : { role: req.body.role, 
                                        forPageUid: req.body.page.uid, 
                                        forPageName: req.body.page.name,
                                        onPageUid: dataRes.data.page.uid, 
                                        onPageName: dataRes.data.page.name 
                                    }
                            
                        }, ['onPageUid'], true)

                        // req.ws.emit(req.body.onPageUid, "new-role-accepted", 
                        //             {   role: req.body.role, 
                        //                 byPageUid: userPage.uid, 
                        //                 byPageName: userPage.name, 
                        //                 onPageUid: req.body.onPageUid, 
                        //                 onPageName: dataRes.data.pageName, })
                    }

                    //dataRes = _.sortBy(dataRes, 'created').reverse()
                    res.json({  error: false })
                }else{
                    console.log("error 1 : dataRes == null")
                    res.json({ error: true, 
                            errorMsg: dataRes.data.msg })
                }
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
                //res.json({ error: true, wallets: null, 
                           //errorMsg: 'An error occured', result: result })
            }) 
    }catch(e){
        console.log("error 3")
        res.json({ error: true,  e:e })
    }

})


router.get('/get-invite-roles', auth,  async (req, res) => {
    //console.log("-------------------------")
    //console.log("/get-invite-roles")

    try{
        let userPage = await Page.findOne( { owner: req.user._id, type: "user" } ).select('uid')
        let nodePath = await core.getNodePath(userPage.uid)
        //console.log("try contact", nodePath + '/broadcast/get-invite-roles', userPage.uid)
        core.broadcaster.post('/broadcast/get-invite-roles', { userPageUid: userPage.uid }, //params
            function(dataRes){ // == then (after response from all instances)
                //if network sent data
                //console.log("dataRes ?", dataRes)
                //if network sent data
                if(dataRes != null){
                    //let finalRes = _.sortBy(finalRes, 'created').reverse()
                    res.json({  error: false, 
                                invites: dataRes })
                }else{
                    console.log("error 1 : dataRes == null")
                    res.json({ error: true, mapPages: null, 
                            errorMsg: 'No page found in this bdd' })
                }
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
            }, 'invites') 
    }catch(e){
        console.log("error 3")
        res.json({ error: true,  e:e })
    }

})


router.get('/get-pending-roles', auth,  async (req, res) => {
    //console.log("-------------------------")
    //console.log("/get-pending-roles")

    try{
        let userPage = await Page.findOne( { owner: req.user._id, type: "user" } ).select('uid')
        let nodePath = await core.getNodePath(userPage.uid)
        //console.log("try contact", nodePath + '/broadcast/get-pending-roles', userPage.uid)
        core.broadcaster.post('/broadcast/get-pending-roles', { 
                                    userPageUid: userPage.uid },
            function(dataRes){ // == then (after response from all instances)
                //if network sent data
                //console.log("dataRes ?", dataRes)
                //if network sent data
                if(dataRes != null){
                    //let finalRes = _.sortBy(finalRes, 'created').reverse()
                    res.json({  error: false, 
                                pendings: dataRes })
                }else{
                    console.log("error 1 : dataRes == null")
                    res.json({ error: true, mapPages: null, 
                            errorMsg: 'No page found in this bdd' })
                }
            },function(result){ // == catch
                console.log("error 2 : fCatch", result)
            }, 'pendings') 
    }catch(e){
        console.log("error 3")
        res.json({ error: true,  e:e })
    }

})




module.exports = router;