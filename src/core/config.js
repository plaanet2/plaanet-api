const config = require('config');

module.exports = {
  ENV: config.get('env') || 'dev',
  INSTANCE_URL: config.get('instance_url'),
  WS_PORT: config.get('bind_ports.socket') || config.get('instance_port_socket'),
  HTTP_PORT: config.get('bind_ports.http') || config.get('instance_port'),
  SERVE_PUBLIC_FOLDER: config.get('serve_public_folder') || false,
  PUBLIC_FOLDER_PATH: config.get('public_folder_path') || './public',
  CORS_BYPASS: config.get('cors_bypass') || null,
  COOKIES_SECURE: config.get('cookies.secure') || false,
  COOKIES_SAME_SITE: config.get('cookies.same_site') || 'Secure',

  Database: {
    HOST: config.get('database.host') || '127.0.0.1',
    PORT: config.get('database.port') || 27017,
    NAME: config.get('database.name') || 'plaanet-default',
    USER: config.get('database.username') || null,
    PASS: config.get('database.password') || null,
    SECURE: config.get('database.secure') || false,
  },

  RootNode: {
    URL: config.get('root_node.url'),
    PORT: config.get('root_node.port'),
  },

  Secrets: {
    COOKIES_KEY: config.get('access_pk') || null,
    CRON_TOKEN: config.get('cron_pk') || null,
  }
};