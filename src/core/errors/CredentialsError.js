module.exports = class CredentialsError extends Error {

  constructor(i18nKey, message) {
    super(message);

    this.name = 'CredentialsError';
    this.httpStatusCode = 403;
    this.i18nKey = i18nKey;
  }

}