module.exports = class EntityDoesNotExistsError extends Error {

  constructor(message, entityId) {
    super(message);

    this.name = 'EntityDoesNotExistsError';
    this.details = { entityId };
    this.httpStatusCode = 404;
  }

}
