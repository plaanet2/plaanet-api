const bodyParser = require('body-parser');

module.exports = async function bodyParserLoader({ expressApp }) {
  // support json encoded bodies
  expressApp.use(bodyParser.json());
  
  // support encoded bodies
  expressApp.use(bodyParser.urlencoded({ extended: true }));
}