const cookieParser = require('cookie-parser');

const Config = require('../config');

module.exports = async function cookiesLoader({ expressApp }) {
  if (!Config.Secrets.COOKIES_KEY) {
    console.log("FATAL: access_pk is not defined. Terminating...");
    process.exit(1);
  }

  expressApp.use(cookieParser(Config.Secrets.COOKIES_KEY));
}