const uuid = require('uuid').v4;

const Instance = require('../../models/Instance');
const isInstanceInitialized = require('../middleware/is-instance-initialized');

module.exports = async function initializedStateLoader({ expressApp }) {
  try {
    const instanceData = await Instance.findOne({ isLocal: true })

    global.store.isInstanceInitialized = instanceData != null;

    if (!global.store.isInstanceInitialized) {
      global.store.initToken = uuid();

      console.log(`INFO: Init token: ${global.store.initToken}
      Use this token to initialize your instance via POST /instance/init and setting
      the X-Init-Token header on the request or via /boot-node in client.
	  Alternatively use the following command: wget --header="X-Init-Token: ${global.store.initToken}" --post-data= http://HOSTNAME:PORT/instance/init
      See docs: TODO ADD DOCS.`);
    }

    // Check that instance is initialized.
    expressApp.use(isInstanceInitialized.unless({
      path: [
        { methods: ['OPTIONS', 'POST'], url: '/instance/init' },
        { method: 'GET', url: '/instance/exists' },
      ]
    }));
  } catch (err) {
    console.log(`ERROR: Cannot retrieve instance initialized state: ${err.message}`);
    global.store.isInstanceInitialized = false;
  }
};
