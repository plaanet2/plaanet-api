const lastUserActivity = require('../middleware/lastUserActivity');

module.exports = async function lastUserActivityLoader({ expressApp }) {
  expressApp.use(lastUserActivity);
}