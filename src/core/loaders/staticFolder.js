const express = require('express');

const Config = require('../config');

module.exports = async function staticFolderLoader({ expressApp }) {
  if (Config.SERVE_PUBLIC_FOLDER) {
    console.log('INFO: Serving public files from express!');
    expressApp.use(express.static('public'));
  }
}