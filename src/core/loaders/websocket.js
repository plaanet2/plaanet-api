const ws = require('../../core/socket');
const authMiddleware = require('../../core/middleware/auth');
const AuthenticationError = require('../errors/AuthenticationError');

// Function that wraps an express middleware to map it to socket.io one
// It will also terminate socket when middleware (more a guard in this case) fails
const wrap = middleware => (socket, next) => {
  const nextFn = (err) => {
    if (err && err instanceof AuthenticationError) {
      // True close the socket, else only the namespace is closed
      // (and an useless socket is left behind)
      return socket.disconnect(true);
    }

    next();
  }

  middleware(socket.request, {}, nextFn)
};

module.exports = async function websocketLoader({ expressApp, ioSocket }) {  
  // This reuses the normal HTTP auth middleware (by wrapping it)
  ioSocket.use(wrap(authMiddleware));
  
  // Initialize Plaanet ws stuff.
  ws.init(ioSocket);

  // Setup middleware to set ws utils in req.
  expressApp.use(function (req, _, next) {
    req.ws = ws;
    next();
  });
}