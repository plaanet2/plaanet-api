const {AuthenticationError, EntityDoesNotExistsError} = require('../errors');

const AuthTokenService = require('../../services/authToken');
const UserService = require('../../services/user');

module.exports = async function authMiddleware(req, _, next) {
  try {
    const authTokenService = new AuthTokenService();
    const userService = new UserService();

    //get the token from the header if present
    const token = await authTokenService.getTokenFromRequest(req);
    if (!token) {
      throw new AuthenticationError(
        'auth.emptyCredentials',
        'Auth token is missing.'
      );
    }

    const isTokenValid = await authTokenService.isTokenValid(token);
    if (!isTokenValid) {
      throw new AuthenticationError(
        'auth.invalidCredentials',
        'Auth token has been tampered or not created by us.'
      );
    }
    
    const tokenPayload = await authTokenService.getTokenPayload(token);
    if (!tokenPayload) {
      throw new AuthenticationError(
        'auth.invalidCredentials',
        'Auth token is invalid. Please generate a new token.'
      );
    }

    const user = await userService.getUserById(tokenPayload._id);
    if (!user) {
      throw new EntityDoesNotExistsError(
        'User specified in auth token does not exists.',
        tokenPayload._id
      );
    }
    
    // Attach user to request object.
    req.user = user;

    // Allow request if user is set.
    next();
  } catch (err) { 
    next(err);
  }
};