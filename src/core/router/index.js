const routes = require('../../routes.js');

module.exports = async function registerRoutes({ expressApp }) {
  for (let i = 0, l = routes.length; i < l; i++) {
    console.log(`DEBUG: Registering endpoint ... ${routes[i].path} => ${routes[i].controller.name}`);
    expressApp.use(routes[i].path, routes[i].controller);
  }
};