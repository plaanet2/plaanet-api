var mongoose = require("mongoose")

var liveSchema = new mongoose.Schema({
    uid: String,
    parentPageUid: String,
    created: Date,
    dateLastMsg: Date, //la date du dernier message
    dateLastOpen: Array,   //enregistre la date d'ouverture 
    categories: Array,
})

var Live = mongoose.model('Live', liveSchema)
module.exports = Live;
