var mongoose = require('mongoose');

var statUrlSchema = new mongoose.Schema({
    date: Date,
    lastDate: Date,
    url: String,
    count: Number,
    inx: Number, //to sort query
    clientDomaineName: String //plaanet.io:444
});

var StatUrl = mongoose.model('statUrl', statUrlSchema);

module.exports = StatUrl;