
const authController = require('./controllers/auth');
const userController = require('./controllers/user');
const adminController = require('./controllers/admin');
const broadcastController = require('./controllers/broadcast');
const instanceController = require('./controllers/instance');
const pageController = require('./controllers/page');
const publicationController = require('./controllers/publication');
const commentController = require('./controllers/comment');
const uploadController = require('./controllers/upload');
const tqwController = require('./controllers/tqw');
const autocompleteController = require('./controllers/autocomplete');
// const surveyController = require('./controllers/survey');
const roomController = require('./controllers/room');
const liveController = require('./controllers/live');
const mailController = require('./controllers/mail');
const fastController = require('./controllers/fast');

module.exports = [
  { path: '/_status', controller: (_, res) => res.end('OK') },
  { path: '/auth', controller: authController },
  { path: '/user', controller: userController },
  { path: '/admin', controller: adminController },
  { path: '/broadcast', controller: broadcastController },
  { path: '/instance', controller: instanceController },
  { path: '/page', controller: pageController },
  { path: '/publication', controller: publicationController },
  { path: '/comment', controller: commentController },
  { path: '/upload', controller: uploadController },
  { path: '/tqw', controller: tqwController },
  { path: '/autocomplete', controller: autocompleteController },
  { path: '/room', controller: roomController },
  { path: '/live', controller: liveController },
  { path: '/mail', controller: mailController },
  { path: '/fast', controller: fastController },
];