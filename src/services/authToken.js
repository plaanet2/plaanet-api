const jwt = require('jsonwebtoken');
const cookie = require('cookie');
const cookieSign = require('cookie-signature');

const Config = require('../core/config');

module.exports = class AuthTokenService {

  constructor() { }
  
  async isTokenValid(authToken) {
    try {
      const decodedToken = jwt.verify(authToken, Config.Secrets.COOKIES_KEY);
      return decodedToken != null;
    } catch (err) {
      return false;
    }
  }

  async getTokenPayload(authToken) {
    if (!this.isTokenValid(authToken)) {
      return null;
    }

    return jwt.decode(authToken);
  }

  async getTokenFromRequest(req) {
    if (!req) {
      return null;
    }

    if (req.headers && req.headers['x-auth-token']) {
      return req.headers['x-auth-token'];
    }

    if (req.headers && req.headers['auth-token']) {
      return req.headers['auth-token'].replace(/Bearer /, '');
    }

    if (req.signedCookies && req.signedCookies['plaanet_access_token']) {
      return req.signedCookies['plaanet_access_token'];
    }

    // For the req object from ws.
    if (req.headers && req.headers['cookie']) {
      const parsedCookies = cookie.parse(req.headers['cookie']);
      if (parsedCookies && parsedCookies['plaanet_access_token']) {
        const cookie = parsedCookies['plaanet_access_token'];

        // Remove prefix added by cookie-parser during setCookie
        if (cookie.startsWith('s:')) {
          return cookieSign.unsign(
            cookie.replace(/^s:/, ''),
            Config.Secrets.COOKIES_KEY
          );
        }

        return cookieSign.unsign(
          cookie,
          Config.Secrets.COOKIES_KEY
        );
      }

      return null;
    }

    return null;
  }

}