const Config = require('../core/config');
const AuthTokenService = require('./authToken');

module.exports = class CronTokenService {

  constructor() {
    this.authTokenService = new AuthTokenService();
  }

  async getTokenFromRequest(req) {
    if (!req) {
      return null;
    }
    
    if (req.query.cronpk) {
      return req.query.cronpk;
    }

    const token = await this.authTokenService.getTokenFromRequest(req);
    if (token) {
      return token;
    }

    return null;
  }

  async tokenMatchsConfig(cronToken) {
    return Config.Secrets.CRON_TOKEN === cronToken;
  }

}