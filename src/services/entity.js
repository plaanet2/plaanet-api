const Instance = require('../models/Instance');
const StatUrl = require('../models/StatUrl');

const fs = require('fs')
const core = require('../core/core');

module.exports = class EntityService {

  constructor() {}

  requireModel(entityType){
    //remplace la premiere lettre par une majuscule
    let entityName = entityType.replace(/^\w/, (c) => c.toUpperCase());
    return require('../models/' + entityName)
  }

  async getEntities(entityType, req, after=()=>{ return [] }) {
    //console.log("getEntities query", req)
    let qry = req.qry 
    let params = {  entityType : entityType,
                    origin : req.origin,
                    key : req.key, //key origin
                    pageUid : req.fromPageUid,
                    query: qry.query,
                    limit: qry.limit,
                    skip: qry.skip,
                    sort: qry.sort,
                    populate: qry.populate
                  }
                  
    try{ //broadcast request (to each instance of the network)
      let fRes = await core.broadcaster.fast('/fast/get-entities', params, //params
        async (foreignEntities) => { // == then (after response from all instances)
          //if network sent data
          if(foreignEntities != null){
            const Model = this.requireModel(entityType)
            let count = await Model.countDocuments(params.query)
            let localEntities = await this.queryEntity(params)
            let allRes = foreignEntities.concat(localEntities)
            let finalRes = after(allRes)

            return {  error: false, 
                      entities: finalRes, 
                      count: count,
                      limit: qry.limit,
                      skip: qry.skip }
          }else{
              return { error: true, entities: null, 
                        errorMsg: 'No ' + entityType + ' found in this bdd' }
          }
        },
        function(result){ // == catch
          console.log("getEntities error 2 : fCatch", result)
          return { error: true, entities: null, result: result }
        },  
        'entities', true) //foreignEntities[id] to concat for each response after forward
    
      return fRes
    }
    catch(e){
        console.log("getEntities error 3", e)
        return { error: true, entities: null, e:e }
    }
  }

  async getEntitiesAsync(entityType, req, after=()=>{ return [] }) {
    //console.log("getEntitiesAsync1 query", req.qry.query )
    let qry = req.qry 
    let query = qry.query 

    //console.log("getEntitiesAsync2 query", query )
    let params = {  entityType : entityType,
                    origin : req.origin,
                    key : req.key, //key origin
                    pageUid : req.fromPageUid,
                    query: qry.query,
                    limit: qry.limit,
                    skip: qry.skip,
                    sort: qry.sort,
                    populate: qry.populate,
                    search: req.search
                  }
                  
    try{ //broadcast request (to each instance of the network)
      core.broadcaster.fastAsync('/fast/get-entities', params, //params
        async (foreignEntities) => { // == then (after response from all instances)
          
        },
        function(error){ // == catch
          console.log("getEntitiesAsync error 2 : fCatch", error)
          //return { error: true, entities: null, result: result }
        },  
        'entities', true) //foreignEntities[id] to concat for each response after forward
    }
    catch(e){
        console.log("getEntitiesAsync error 3", e)
        //return { error: true, entities: null, e:e }
    }
  }

  async queryEntity(qry){
    let limit = qry.limit ? qry.limit : 100
    limit = limit > 100 ? 100 : limit

    const Model = this.requireModel(qry.entityType)
    
    //reconstruit le regex (ne passe pas dans les request entre serveur)
    if(qry.query['$or'] != null 
    && qry.query['$or'][0].name != null 
    && qry.search != null){
      qry.query['$or'][0].name = new RegExp(".*"+qry.search.toLowerCase().trim(), "i")
    }

    try{
      let select = ["-accessToken", "-password", "-email"] //ne jamais renvoyer les token, ni les mot de passe
      let entities = await Model.find(qry.query)
                                .select(select)
                                .sort(qry.sort)
                                .skip(qry.skip*limit)
                                .limit(limit)
                                .populate(qry.populate)

      let totalRes = await Model.countDocuments(qry.query)
      //console.log("entities", entities.length)
      return { entities : entities, totalRes : totalRes }
    }catch(e){
      console.log("***************error", e)
      return []
    }
  }

}