const Instance = require('../models/Instance');
const StatUrl = require('../models/StatUrl');
const { User } = require('../models/User');
const slugify = require('slugify');

module.exports = class InstanceService {

  constructor() {

  }

  async getLocalInstance() {
    return Instance
    .findOne({ isLocal: true })
    .exec();
  }
  
  async getInstances() {
    return Instance
    .find()
    .select('-nodeKey')
    .exec();
  }

  async getInstanceById(id) {
    return Instance
      .findById(id)
      .select('-nodeKey')
      .exec();
  }

  async getInstanceByUri(url, port) {
    return Instance
      .findOne({ url, port })
      .select('-nodeKey')
      .exec();
  }

  async getInstanceByName(name) {
    return Instance
      .findOne({ name })
      .select('-nodeKey')
      .exec();
  }
  
  async getInstanceByNameWithNodeKey(name) {
    return Instance
      .findOne({ name })
      .exec();
  }

  async initializeInstance(instanceOpts) {
    // TODO: Validate instanceOpts by using a schema.

    const instance = new Instance({
      isLocal: true,
      nbUserGlobal: 0,
      name: slugify(instanceOpts.name, {
        lower: true,
        remove: /[*+~.()'"!:@]/g,
      }),
      nodeKey: instanceOpts.nodeKey,
      url: instanceOpts.url,
      port: instanceOpts.port,
      portSocket: instanceOpts.portSocket,
      clientUrl: instanceOpts.clientUrl,
      clientPort: instanceOpts.clientPort,
      city: instanceOpts.city,
      state: instanceOpts.state,
      position: {
        type: 'Point',
        coordinates: instanceOpts.coordinates,
      },
    });

    return instance.save();
  }

  async findInstanceNearCoordinates(coordinates) {
    return Instance
      .findOne({
        'position': {
          '$near': {
            $geometry: {
              type: 'Point',
              coordinates,
            }
          }
        }
      })
      .select('-nodeKey')
      .exec();
  }

  async initDayStat(){

    let date1D = new Date()
    //date1D.setDate(date1D.getDate() - 1)

    //console.log("SERVICE - instance.initDayStat", date1D)
    let nbExist = 0
    let nbTotal = 0
    let urlsEnabled = this.getStatUrlEnabled()

    let nbDays = 15 // = 0 pour aujourd'hui seulement OU >0 pour simuler plusieurs jours
    for(let i = nbDays; i >= 0; i--){
        let day = new Date()
        let today = new Date(day.getFullYear(), day.getMonth(), day.getDate(), 1, 0, 0)
        let dateR = new Date()
        let dateL = new Date()
        dateR.setDate(dateR.getDate() - i)
        dateL.setDate(dateL.getDate() - i - 1)
        //console.log("New stat to init", dateL, dateR)
        await Promise.all(urlsEnabled.map(async (url, x) => {

            //console.log("New stat url", url, dateR.getFullYear(), dateR.getMonth()+1, dateR.getDate())
            let stat = await StatUrl.findOne({ url: url, '$and' : [
                                                            { date: { '$gt': dateL } },
                                                            { date: { '$lt': dateR } },
                                                            
                                                         ]
                                            }).sort('date')
            
            //let stat = await StatUrl.findOne({ url: url, date: dateR }).sort('date')

            if(stat == null){
                let rand = 0 //nbDays > 0 ? Math.floor(Math.random() * Math.floor(10)) : 0
                let newStat = new StatUrl()
                let date = new Date(new Date(dateR.getFullYear(), dateR.getMonth(), dateR.getDate(), 1, 0, 0))
                newStat.clientDomaineName = "plaanet.io"
                newStat.date =  date
                newStat.lastDate =  date
                newStat.url = url
                newStat.count = rand
                newStat.inx = x
                await newStat.save()
                console.log("New stat inited", url, newStat.count)
                //return res.json({ error: false })
            }else{
                nbExist++
                //console.log("New stat already exists", url, dateR)
                //return res.json({ error: false, msg: 'This day is already inited for this url', url })
            }
            nbTotal++
        }))
    }
    //console.log("Stats already exists :", nbExist, "/", nbTotal)
  }

  // async checkStatAllDays(){

  //   let nbDays = 50 // = 0 pour aujourd'hui seulement OU >0 pour simuler plusieurs jours
  //   for(let i = nbDays; i >= 0; i--){
  //     let dateR = new Date()
  //     let dateL = new Date()
  //     dateR.setDate(dateR.getDate() - i)
  //     dateL.setDate(dateL.getDate() - i - 1)

  //     let stat = await StatUrl.findOne({ url: url, '$and' : [
  //             { date: { '$lt': dateR } },
  //             { date: { '$gt': dateL } },
  //     ]
  //     }).sort('date')
  //   }
  // }
  
  getStatUrlEnabled(){
    let u = [
      '/home',
      '/manifest',
      '/login',
      '/register',
      '/select-node',
      '/info-nodes',
      '/status',
      '/legals',
      '/cgu',

      '/stream',
      '/stream-explore',
      '/private',
      '/private/sendto',
      '/find-assembly',
      '/room-all',
      '/room', 
      '/room/publish',
      '/create-page',
      '/live',
      '/dailymail'
    ]
    return u.reverse()
  }


}