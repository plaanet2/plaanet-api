const nodemailer = require("nodemailer");
const config = require('config');
// const StatUrl = require('../models/StatUrl');
const { User } = require('../models/User');
// const InstanceService = require('../services/instance');
const UserService = require('../services/user');
const PageService = require('../services/page');
const NotifService = require('../services/notification');

const mailsDailyNotif = require('../mails/daily_notif.js');

module.exports = class MailService {

  constructor() { 
    this.mailActivated = config.get("mail_activated") ? config.get("mail_activated") : false
    this.log = config.get("mail_log") ? config.get("mail_log") : false
    this.auth = config.get("mail_auth")
    this.mailHost = config.get("mail_host")
    this.mailPort = config.get("mail_port")
    this.from = config.get("mail_from")

    this.transporter = nodemailer.createTransport({
      host: this.mailHost,
      port: this.mailPort,
      auth: this.auth,
      secure: false // true for 465, false for other ports
    });
  }

  async sendMail(sendTo, subject, textHtml){

    if(!this.mailActivated) return
    
    //ne pas envoyer de mail à n'importe qui pendant les tests
    if(config.get("env") == 'dev') 
      sendTo = config.get("mail_email_test")

    // send mail with defined transport object
    try{
      let info = await this.transporter.sendMail({
        from: this.from, // sender address
        to: sendTo, // list of receivers
        subject: subject, // Subject line
        html: textHtml, // html body
      });

      //if(this.log) console.log("Email sent to", sendTo, info, "id:", info.messageId)
      return { success: true, mailId: info.messageId }
    }
    catch(e){
      if(this.log) console.log("Error sending email to", sendTo)
      if(this.log) console.log("Error :", e)
      
      return { success: false, error: e }
    }
  }

  async sendDailyNotif(params){
    //console.log("sendDailyNotif", params)
    //récupère le html généré avec les params
    const html = mailsDailyNotif(params)

    let resMail = { success: true }
    if(config.get("env") == 'prod')
      resMail = await this.sendMail(params.email, "Notifications", html) // pour les test : { success: true } // 

    //en cas d'erreur, supprimer le mail de ce user, pour ne pas lui renvoyer un autre mail
    //sur cette adresse qui ne fonctionne pas
    //ex : 450 4.1.2 <mr.alpha.tango@protonmail.comm>: Recipient address rejected: Domain not found
    if(resMail.success == false 
    && resMail.error.responseCode == 450){
      let userService = new UserService()
      let r = await userService.unsetEmailById(params.userId)
      console.log("deleted email for user", r)
      return resMail
    }

    await this.traceDailyEmailChecked(params.userId)
    return resMail
  }

  async traceDailyEmailChecked(userId){
    //console.log("traceDailyEmailChecked", userId)
    let userService = new UserService()
    let user = await userService.getUserById(userId)
    if(user != null){
      user.lastDailyEmailDate = new Date()
      user.save()
    }
  }

  async isEmailSent(params){
    let date1D = new Date()
    date1D.setDate(date1D.getDate() - 1)
    return params.lastDailyEmailDate > date1D
  }

  async getNotifMailData(user){
    let pageService = new PageService()
    let notifService = new NotifService()

    let page = user.page != null ? user.page : (user.pages[0] != null ? user.pages[0] : null)
    if(page == null){
      console.log("**********error", user.name, "has no page found", user)
      return {  userId: user._id, 
                name: user.name, 
                email: user.email, 
                lastDailyEmailDate: user.lastDailyEmailDate, 
                nbNotif: 0, 
                nbLiveMsgNotRead: 0, 
                nbNotifTotal: 0,
                sent: false
              }
    }
    //récupère la liste des pages dont il est membre (ou ami avec)
    const memberPages = await pageService.getMemberPages(user.pages[0].uid)
    let nbLiveMsgNotRead = 0
    //console.log("memberPages", memberPages)
    //si une page a une alerte : il y a un live message reçu
    memberPages.forEach((page)=>{ if(page.alert == true) nbLiveMsgNotRead++ })
    //récupère les notifications liées à la page user du user 
    let notifs = await notifService.getNotificationsNotRead(user.pages[0].uid, user.pages[0].dateReadNotif)
    //enregistre les détails du mail à envoyer (s'il y a des notifs)
    return {  userId: user._id, 
              name: user.name, 
              email: user.email, 
              lastDailyEmailDate: user.lastDailyEmailDate, 
              nbNotif: notifs.length, 
              nbLiveMsgNotRead: nbLiveMsgNotRead, 
              nbNotifTotal: notifs.length + nbLiveMsgNotRead,
              sent: false
            }
  }

  async getCurrentLimitFor24h(){
    let date1D = new Date()
    date1D.setDate(date1D.getDate() - 1)
    
    let allUsersWithMail = await User.find({ email: { '$exists': true }})
    let totalUserWithMail = allUsersWithMail.length
    //meme condition + la date
    let allUsersNotifiable = await User.find({ email: { '$exists': true },
                                              allowEmail: true,
                                              lastDailyEmailDate: { '$lt' : date1D } 
                                          })

    // let max = 10
    let totalNotifiable = allUsersNotifiable.length
    let limit = (totalUserWithMail < 10) ? totalUserWithMail : 10 //parseInt(totalNotifiable / 20) //20 heures => pour être sur d'envoyer à tout le monde en 24h
    
    let res = { limit : limit, 
                totalUserWithMail: allUsersWithMail.length,
                totalNotifiable: totalNotifiable
              }
    
    //console.log("getCurrentLimitFor24h", res)
    return res
  }
}